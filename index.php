<div class="row mb-2">
    <div class="col-sm-6">
        <h1 class="m-0">Home</h1>
    </div>
    <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Dashboard Admin</a></li>
            <li class="breadcrumb-item active">Home</li>
        </ol>
    </div>
</div>

<section class="content">
    <div class="container-fluid">
        <div class="row">

            <div class="col-lg-6 col-sm-12">
                <div class="small-box bg-info">
                    <div class="inner">
                        <h3>
                            <?php echo $data_pengguna ?>
                        </h3>
                        <p>Pengguna</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-person"></i>
                    </div>
                </div>
            </div>

            <div class="col-lg-6 col-sm-12">
                <div class="small-box bg-success">
                    <div class="inner">
                        <h3>
                            <?php echo $data_admin ?>
                        </h3>
                        <p>Jumlah Admin</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-person"></i>
                    </div>
                </div>
            </div>

            <div class="col-lg-6 col-sm-12">
                <div class="card bg-white">
                    <div class="card-body">
                        <p class="text-center text-uppercase text-bold">surat masuk (<?php echo $data_sm ?>)</p>
                        <canvas id="barChartMasuk"></canvas>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-sm-12">
                <div class="card bg-white">
                    <div class="card-body">
                        <p class="text-center text-uppercase text-bold">surat keluar (<?php echo $data_sk ?>)</p>
                        <canvas id="barChartKeluar"></canvas>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
    <?php
    $sql_masuk = "SELECT YEAR(tanggal_terima) as Tahun, COUNT(*) as JumlahSuratMasuk FROM surat_masuk GROUP BY YEAR(tanggal_terima)";
    $result_masuk = $conn->query($sql_masuk);

    $data_surat_masuk = array();
    while ($row = $result_masuk->fetch_assoc()) {
        $data_surat_masuk[] = $row;
    }

    $sql_keluar = "SELECT YEAR(tanggal_surat) as Tahun, COUNT(*) as JumlahSuratKeluar FROM surat_keluar GROUP BY YEAR(tanggal_surat)";
    $result_keluar = $conn->query($sql_keluar);

    $data_surat_keluar = array();
    while ($row = $result_keluar->fetch_assoc()) {
        $data_surat_keluar[] = $row;
    }

    $tahun_masuk = array();
    $jumlah_masuk = array();
    foreach ($data_surat_masuk as $data) {
        $tahun_masuk[] = $data['Tahun'];
        $jumlah_masuk[] = $data['JumlahSuratMasuk'];
    }

    $tahun_keluar = array();
    $jumlah_keluar = array();
    foreach ($data_surat_keluar as $data) {
        $tahun_keluar[] = $data['Tahun'];
        $jumlah_keluar[] = $data['JumlahSuratKeluar'];
    }
    ?>

    <script>
        var tahunMasuk = <?php echo json_encode($tahun_masuk); ?>;
        var jumlahSuratMasuk = <?php echo json_encode($jumlah_masuk); ?>;
        var tahunKeluar = <?php echo json_encode($tahun_keluar); ?>;
        var jumlahSuratKeluar = <?php echo json_encode($jumlah_keluar); ?>;

        var ctxMasuk = document.getElementById('barChartMasuk').getContext('2d');
        var myChartMasuk = new Chart(ctxMasuk, {
            type: 'bar',
            data: {
                labels: tahunMasuk,
                datasets: [{
                    label: 'Jumlah Surat Masuk',
                    data: jumlahSuratMasuk,
                    backgroundColor: 'rgba(54, 162, 235, 0.2)',
                    borderColor: 'rgba(54, 162, 235, 1)',
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    y: {
                        beginAtZero: true,
                        ticks: {
                            callback: function(value) { if (Number.isInteger(value)) { return value; } }
                        }
                    }
                }
            }
        });

        var ctxKeluar = document.getElementById('barChartKeluar').getContext('2d');
        var myChartKeluar = new Chart(ctxKeluar, {
            type: 'bar',
            data: {
                labels: tahunKeluar,
                datasets: [{
                    label: 'Jumlah Surat Keluar',
                    data: jumlahSuratKeluar,
                    backgroundColor: 'rgba(255, 99, 132, 0.2)',
                    borderColor: 'rgba(255, 99, 132, 1)',
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    y: {
                        beginAtZero: true,
                        ticks: {
                            callback: function(value) { if (Number.isInteger(value)) { return value; } }
                        }
                    }
                }
            }
        });
    </script>

</section>
